const popularData = [
  {
    id: '1',
    image: require('../images/pizza1.png'),
    title: 'Pizza Especial',
    weight: '540 gr',
    rating: '5.0',
    price: 233.99,
    sizeName: 'Mediano',
    calories: '139',
    deliveryTime: 30,
    ingredients: [
      {
        id: '1',
        name: 'ham',
        image: require('../images/ham.png'),
      },
      {
        id: '2',
        name: 'tomato',
        image: require('../images/tomato.png'),
      },
      {
        id: '3',
        name: 'cheese',
        image: require('../images/cheese.png'),
      },
      {
        id: '4',
        name: 'garlic',
        image: require('../images/garlic.png'),
      },
    ],
  },
  {
    id: '2',
    image: require('../images/pizza2.png'),
    title: 'Pizza Vegetariana',
    weight: '450 gr',
    rating: '4.0',
    price: 195.99,
    sizeName: 'Chica',
    calories: 120,
    deliveryTime: 40,
    ingredients: [
      {
        id: '1',
        name: 'cheese',
        image: require('../images/cheese.png'),
      },
      {
        id: '2',
        name: 'garlic',
        image: require('../images/garlic.png'),
      },
    ],
  },
  {
    id: '3',
    image: require('../images/pizza3.png'),
    title: 'Pizza Napolitana',
    weight: '700 gr',
    rating: '5.0',
    price: 389.99,
    sizeName: 'Grande',
    calories: 340,
    deliveryTime: 20,
    ingredients: [
      {
        id: '1',
        name: 'tomato',
        image: require('../images/tomato.png'),
      },
      {
        id: '2',
        name: 'cheese',
        image: require('../images/cheese.png'),
      },
    ],
  },
];

export default popularData;